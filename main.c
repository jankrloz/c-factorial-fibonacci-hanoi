#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

long int fibIter (long int);
long int fibRecursivo (long int);
long int facIter (long int);
void TH (int, char, char, char);

double performancecounter_diff(LARGE_INTEGER *a, LARGE_INTEGER *b);


double performancecounter_diff(LARGE_INTEGER *a, LARGE_INTEGER *b){
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    return (double)(a->QuadPart - b->QuadPart) / (double)freq.QuadPart;
}

long int fibIter (long int n){
    long int r; 
    long int n1 = 0; long int n2 = 1;
    int i; 
    
    for (i = 0; i < n - 1; i++){
        r = n2 + n1;
        n1 = n2;
        n2 = r;
    }
    
    return r; 
}

long int fibRecursivo (long int n){
    if (n < 2)
       return n;
    return fibRecursivo (n-1) + fibRecursivo (n-2);
}

long int facIter (long int n){
     long int r = 1;
     int i;
     for (i = 1; i <= n; i++){
         r *= i;
     }
     return r;
}

long int facRecursivo (long int n){
     if (n == 1 || n == 0)
        return 1;
     return (n * facRecursivo(n-1));
}

void TH (int n, char o, char a, char d){
     if (n == 1){
        printf ("Mover el disco %d, de %c a %c\n", n, o, d);
     }
     else{
        TH (n-1, o, d, a);
        printf ("Mover el disco %d, de %c a %c\n", n, o, d);
        TH (n-1, a, o, d);
     }
}

int main(int argc, char *argv[])
{
    LARGE_INTEGER t_ini, t_fin;
    double secs;
    long int numUser;
    long int res;
    int opc, opc1;
    int value; int val;
    val = 1; value = 1;
    while (val == 1){
    system ("CLS");
    printf ("\n\nMENU\n");
    printf ("\n\t1.- Factorial");
    printf ("\n\t2.- Serie de Fibonacci");
    printf ("\n\t3.- Torres de Hanoi");
    printf ("\n\t4.- Salir");
    printf ("\n\n\t\tOpcion: "); scanf ("%d", &opc);
    fflush (stdin);
    switch (opc){
           case 1:
                value = 1;
                while (value == 1){
                    system ("CLS");
                    printf ("\n\t1.- Forma iterativa");
                    printf ("\n\t2.- Forma recursiva");
                    printf ("\n\n\t\tOpcion: "); scanf ("%d", &opc1);
                    fflush (stdin);
                    switch (opc1){
                           case 1:
                                printf ("\nIngrese un numero para calcular su factorial: "); scanf ("%d", &numUser);
                                fflush (stdin);
                                QueryPerformanceCounter(&t_ini);
                                res = facIter(numUser);
                                QueryPerformanceCounter(&t_fin);
                                
                                printf ("\n\nResultado: %d", res);
                                
                                secs = performancecounter_diff(&t_fin, &t_ini);
                                printf("\n%.9f segundos\n\n", secs);
                                system ("PAUSE");
                                value = 0;
                                break;
                           case 2:
                                printf ("\nIngrese un numero para calcular su factorial: "); scanf ("%d", &numUser);
                                fflush (stdin);
                                QueryPerformanceCounter(&t_ini);
                                res = facRecursivo (numUser);
                                QueryPerformanceCounter(&t_fin);
                                
                                printf ("\n\nResultado: %d", res);
                                
                                secs = performancecounter_diff(&t_fin, &t_ini);
                                printf("\n%.9f segundos\n\n", secs);
                                system ("PAUSE");
                                value = 0;
                                break;
                           default:
                                printf ("\n\nOpcion invalida");
                                system ("PAUSE");
                    }
                }
                break;
           case 2:
                value = 1;
                while (value == 1){
                    system ("CLS");
                    printf ("\n\t1.- Forma iterativa");
                    printf ("\n\t2.- Forma recursiva");
                    printf ("\n\n\t\tOpcion: "); scanf ("%d", &opc1);
                    fflush (stdin);
                    switch (opc1){
                           case 1:
                                printf ("\nIngrese un numero para calcular su serie: "); scanf ("%d", &numUser);
                                fflush (stdin);
                                QueryPerformanceCounter(&t_ini);
                                res = fibIter(numUser);
                                QueryPerformanceCounter(&t_fin);
                                
                                printf ("\n\nResultado: %d", res);
                                
                                secs = performancecounter_diff(&t_fin, &t_ini);
                                printf("\n%.9f segundos\n\n", secs);
                                system ("PAUSE");
                                value = 0;
                                break;
                           case 2:
                                printf ("\nIngrese un numero para calcular su serie: "); scanf ("%d", &numUser);
                                fflush (stdin);
                                QueryPerformanceCounter(&t_ini);
                                res = fibRecursivo (numUser);
                                QueryPerformanceCounter(&t_fin);
                                
                                printf ("\n\nResultado: %d", res);
                                
                                secs = performancecounter_diff(&t_fin, &t_ini);
                                printf("\n%.9f segundos\n\n", secs);
                                system ("PAUSE");
                                value = 0;
                                break;
                           default:
                                printf ("\n\nOpcion invalida");
                                system ("PAUSE");
                    }
                }
                break;
           case 3:
                system ("CLS");
                printf ("\nIngrese un numero de discos: "); scanf ("%d", &numUser);
                printf ("\n");
                QueryPerformanceCounter(&t_ini);
                TH (numUser, 'A', 'B', 'C');
                QueryPerformanceCounter(&t_fin);
                printf ("\n");
                secs = performancecounter_diff(&t_fin, &t_ini);
                printf("\n%.9f segundos\n\n", secs);
                system ("PAUSE");
                break;
           case 4: 
                val = 0;
                break;
           default:
                   printf ("\n\nOpcion invalida");
                   system ("PAUSE");
    }
    }
    system("PAUSE");	
    return 0;
}
